package poste;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import poste.Colis;
import poste.Lettre;
import poste.Recommandation;
import poste.SacPostal;
import poste.ColisExpress;
import poste.ColisExpressInvalide;


class TestLettres {
	
	Lettre l1,l2;
	Colis c1;
	ColisExpress ce1,ce2;
	SacPostal sac1;
	
	private static float tolerancePrix=0.001f;
	private static float toleranceVolume=0.0000001f;
	
	@BeforeEach
	public void setUp() throws Exception{
		l1 = new Lettre("Le pere Noel",
				"famille Kirik, igloo 5, banquise nord",
				"7877", 25, 0.00018f, Recommandation.un, false);
		l2 = new Lettre("Le pere Noel",
				"famille Kouk, igloo 2, banquise nord",
				"5854", 18, 0.00018f, Recommandation.deux, true);
		c1 = new Colis("Le pere Noel", 
				"famille Kaya, igloo 10, terres ouest",
				"7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200); 
		ce1 = new ColisExpress("Le pere Noel",
				"famille Bins, igloo 15, terres sud",
				"3409", 25 ,0.018f, Recommandation.deux,"cadeau express",150, true);
		ce2 = new ColisExpress("Le pere Noel",
				"famille kirok, igloo 45, terres sud",
				"3055", 10 ,0.018f, Recommandation.zero,"cadeau express2",100, false);
		
		sac1 = new SacPostal();
		sac1.ajoute(l1);
		sac1.ajoute(l2);
		sac1.ajoute(c1);
	}
	
		
	@Test
	public void testToString() {
		assertEquals("Lettre 7877/famille Kirik, igloo 5, banquise nord/1/ordinaire",l1.toString());
		assertEquals("Lettre 5854/famille Kouk, igloo 2, banquise nord/2/urgence",l2.toString());
		assertEquals("Colis 7877/famille Kaya, igloo 10, terres ouest/2/0.02/200.0",c1.toString());
	}
	
	@Test
	public void testAffranchissement() {
		assertEquals(1.0f,l1.tarifAffranchissement(),tolerancePrix);
		assertEquals(2.3f,l2.tarifAffranchissement(),tolerancePrix);
		assertEquals(3.5f,c1.tarifAffranchissement(),tolerancePrix);
	}
	
	@Test
	public void testRemboursement() {
		assertEquals(1.5f,l1.tarifRemboursement(),tolerancePrix);
		assertEquals(15.0f,l2.tarifRemboursement(),tolerancePrix);
		assertEquals(100.0f,c1.tarifRemboursement(),tolerancePrix);
	}
	
	@Test
	public void testColisExpress() {
		assertTrue(ce1.getPoids()<30);
		assertTrue(ce2.getPoids()<30);
		assertEquals(33.0f,ce1.tarifAffranchissement(),tolerancePrix);
		assertEquals(30.0f,ce2.tarifAffranchissement(),tolerancePrix);
	}
	@Test
	public void testColisExpressTropLourd() {
		assertThrows(ColisExpressInvalide.class, () -> {
			new ColisExpress("expediteur","adresse","3285",50,0.018f,Recommandation.zero,"objet",100, false);
		});

	}
	@Test
	public void testSacs() {
		assertEquals(116.5f,sac1.valeurRemboursement(),tolerancePrix);
		assertEquals(0.025359999558422715f,sac1.getVolume(),toleranceVolume);
		SacPostal sac2 = sac1.extraireV1("7877");
		assertEquals(0.02517999955569394f,sac2.getVolume(),toleranceVolume);
	}
	
	@Test
	public void testExtractionV2() {
		SacPostal sac2 = sac1.extraireV2("7877");
		assertEquals(0.02517999955569394f,sac2.getVolume(),toleranceVolume);
	}
	
	
}